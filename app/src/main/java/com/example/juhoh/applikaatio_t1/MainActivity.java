package com.example.juhoh.applikaatio_t1;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity {

    Context context = null;
    TextView fileOutput;
    EditText newFilenameInput;
    EditText filenameInput;
    EditText newTextInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        fileOutput = (TextView) findViewById(R.id.textView);
        newFilenameInput = (EditText) findViewById(R.id.newFilenameInput);
        filenameInput = (EditText) findViewById(R.id.filenameInput);
        newTextInput = (EditText) findViewById(R.id.newTextInput);
    }

    public void writeFile(View v) {
        try {
            String filename = String.valueOf(newFilenameInput.getText());
            OutputStreamWriter osw = new OutputStreamWriter(context.openFileOutput(filename, Context.MODE_PRIVATE));
            String s = "";
            s = newTextInput.getText().toString();
            osw.write(s);
            osw.close();
            newFilenameInput.getText().clear();
            newTextInput.getText().clear();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printFile(View v) {
        try {
            String filename = String.valueOf(filenameInput.getText());
            InputStream ins = context.openFileInput(filename);
            BufferedReader br = new BufferedReader(new InputStreamReader(ins));
            String s = "";
            fileOutput.setText(null);
            while ((s = br.readLine()) != null) {
                fileOutput.append(s + "\n");
            }
            ins.close();
            filenameInput.getText().clear();
        } catch (IOException e) {
            Log.e("IOException", "Input error");
        }
    }
}
